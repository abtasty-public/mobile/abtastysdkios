Pod::Spec.new do |s|
  s.name             = 'ABTastySdk'
  s.version          = '1.2'
  s.summary          = 'AB Tasty server side ABTastySdk'

  s.description      = <<-DESC
  The ABTastySDK project is an iOS framework that helps you create server side tests on your native iOS app.
                       DESC

  s.homepage         = 'https://www.abtasty.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ABTasty' => 'adel@abtasty.com' }
  s.source           = { :git => 'https://gitlab.com/abtasty-public/mobile/abtastysdkios.git', :tag => s.version }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_frameworks = 'ABTastySdk.framework'
end
