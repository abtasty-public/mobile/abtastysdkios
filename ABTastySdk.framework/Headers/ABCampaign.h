//
//  ABCampaign.h
//  ABTastySdk
//
//  Created by ABTasty on 15/02/2018.
//  Copyright © 2018 ABTasty. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 
 `ABCampaign` represent Object campaign
 
 */
@interface ABCampaign : NSObject

/*
  id of the campaign
 */
@property (nonatomic, strong) NSString * campaignId;


/*
  id of the variation
 */
@property (nonatomic, strong) NSString * variationId;


/**
  Create instance for ABCampaign
 
  @param pCampaignId NSString id of the campaign
 
  @param pVariationId NSString id of the variation
 
  @return instance
 */
- (instancetype)initCampaign:(NSString*)pCampaignId andVariationId:(NSString*)pVariationId;


@end
