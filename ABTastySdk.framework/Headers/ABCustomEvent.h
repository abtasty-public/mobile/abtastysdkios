//
//  ABCustomEvent.h
//  ABTastySdk
//
//  Created by ABTasty on 13/02/2018.
//  Copyright © 2018 ABTasty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABTracking.h"


/**
 
 `ABCustomEvent` class represent Custom Event
 
 */

@interface ABCustomEvent : ABTracking 


/**
  Label of the custom event tracked as displayed in the reporting . required
 */
@property (nonatomic, strong) NSString * goalName;


/**
   Value attached to the custom tracking event. Only NSString or NSNumber are accpeted. optional
 */
@property (nonatomic, strong) id value;


@end
