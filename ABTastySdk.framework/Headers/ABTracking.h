//
//  ABTracking.h
//  ABTastySdk
//
//  Created by ABTasty on 31/01/2018.
//  Copyright © 2018 ABTasty. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 Devices Type
 */
typedef NS_ENUM(NSInteger, ABDeviceType) {
	/// Mobile
    ABMobile,
    
    /// Tablet
	ABTablet
};


/// :nodoc:
@protocol ABTrackingProtocol <NSObject>


@end


/**
 
 `ABTracking` class represent generik event object
 */


@interface ABTracking : NSObject <ABTrackingProtocol>

 /**
 Device used by the visitor to view the test. The devices currently
 */
@property (nonatomic, assign) ABDeviceType  device_type;


/**
 interface or page from where the event has been created required
 */
@property (nonatomic, strong) NSString * interfaceName;


/**
 longitude of the device for geolocation optional
 */
@property (nonatomic, assign) double          longitude;

/**
 latitude of the device for geolocation optional
 */
@property (nonatomic, assign) double          latitude;

/**
 current ip of the device optional
 */
@property (nonatomic, strong) NSString * ipAddress;

/**
  Create ABTracking object
 
  @param pDeviceType Device used by the visitor to view the test. The devices currently
 
  @param pLong double longitude of the device for geolocation
 
  @param pLat  latitude of the device for geolocation
 
  @param pIpAddress NSString current ip of the device
 
  @param pInterfaceName NSString interface or page from where the event has been created

  @return Instancetype for tracker
 */
- (instancetype)initABTracking:(ABDeviceType)pDeviceType longtitude:(double)pLong
                    latitude  :(double)pLat
                    ipAddress :(NSString*)pIpAddress
                    interfaceName:(NSString*)pInterfaceName;



@end
