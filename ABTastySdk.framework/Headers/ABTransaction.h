//
//  ABTransaction.h
//  ABTastySdk
//
//  Created by ABTasty on 31/01/2018.
//  Copyright © 2018 ABTasty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABTracking.h"


/**
 
 `ABMicroTransaction` class represent Micro Transaction
 
 */

@interface ABMicroTransaction :NSObject

/**
  Label of the product. required
 */
@property (nonatomic, strong) NSString * name;

/**
  Price of the item expressed in cents.required
 */
@property (nonatomic, assign) int  price;

/**
  Identifier of the item. optional
 */
@property (nonatomic, strong) NSString * sku;

/**
  Category of product the item belongs to.
 */
@property (nonatomic, strong) NSString * category;

/**
  Number of identical products purchased at the same time on the mobile app. optional
 */
@property (nonatomic, assign) int  quantity;

/**
  Tax amount applied to the item. optional
 */
@property (nonatomic, assign) int  tax_amount;

/**
 Percentage of tax applied to the item. optional
*/
@property (nonatomic, assign) float tax_rate;

@end



/**
 
 `ABMicroTransaction` class represent Transaction
 
 */

@interface ABTransaction : ABTracking


/**
  Affiliation of the Transaction as displayed in the reporting. required
 */
@property (nonatomic, strong) NSString *affiliation; //required

/**
  Unique identifier of the transaction.required
 */
@property (nonatomic, strong) NSString *transaction_id; //required

/**
  Total amount of money spent by the visitor expressed in cents. required
 */
@property (nonatomic, assign) double revenue; //required

/**
  Shipping cost - expressed in cents.required
 */
@property (nonatomic, assign) double shipping; //required

/**
  Total tax expressed in cents. optional
 */
@property (nonatomic, assign) double tax_total;

/**
  Means of payment. optional
 */
@property (nonatomic, strong) NSString *payment_method;

/**
  	Discount in cents, if applicable. optional
 */
@property (nonatomic, assign) double discount;


/**
  Name of the money used for the payment. required
 */
@property (nonatomic, strong) NSString *currencyName; //required

/**
  Change rate between USD in cents (used as a reference) and the money used for the payment. required
 */
@property (nonatomic, assign) float currencyRate; //required


/**
  Total number of products purchased on the mobile app. optional
 */
@property (nonatomic, assign) double items_count; //required


/**
  Products purchased on the web site/mobile app. optional
 */
@property (nonatomic, strong) NSMutableArray<ABMicroTransaction*> *microTransactions;


@end






