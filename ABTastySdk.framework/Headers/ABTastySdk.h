//
//  ABTastySdk.h
//  ABTastySdk
//
//  Created by ABTasty on 22/01/2018.
//  Copyright © 2018 ABTasty. All rights reserved.
//

#import <UIKit/UIKit.h>


//! Project version number for ABTastySdk.
FOUNDATION_EXPORT double ABTastySdkVersionNumber;

//! Project version string for ABTastySdk.
FOUNDATION_EXPORT const unsigned char ABTastySdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ABTastySdk/PublicHeader.h>


#import <ABTastySdk/ABTasty.h>
#import <ABTastySdk/ABActionTracking.h>
#import <ABTastySdk/ABTransaction.h>
#import <ABTastySdk/ABCustomEvent.h>
#import <ABTastySdk/ABVisitEvent.h>
#import <ABTastySdk/ABCampaign.h>




