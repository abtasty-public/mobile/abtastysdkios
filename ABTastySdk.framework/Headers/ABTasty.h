//
//  ABTasty.h
//  ABTastySdk
//
//  Created by ABTasty a on 22/01/2018.
//  Copyright © 2018 ABTasty. All rights reserved.
//  Version  "1.1.2"

#import <Foundation/Foundation.h>
#import "ABTracking.h"

@class ABConfigTracking;
@class ABCampaign;
@class ABTracking;


/**
 
`ABTasty` class helps you create server side tests on your native iOS app.
 
 You can use this class to access AB Tasty endpoints, which can generate a unique visitor ID, allocate a visitor to a test, and push visits and conversions events in order to help you analyze the outcomes of your campaigns.
 */


@interface ABTasty : NSObject

/**
  Visitor Id
*/
@property (nonatomic, readonly) NSString * _Nonnull visitorId;

/**
  Config for tracking
*/
@property (nonatomic, strong) ABConfigTracking * _Nullable configTracking;



/**
 Dislpay Logs , By default the value is NO
 */
@property (nonatomic, assign) BOOL displayLogs;


/**
 SharedInstance
 */
+ (instancetype _Nonnull )sharedInstance;


///-----------------------------------------
/// @name Initialization
///-----------------------------------------

/**
  Start ABTastySdk with the given application identifier
 
  @param appId NSString Access key provided by AB Tasty
 */
- (void)startABTastySdk:(NSString*_Nonnull)appId;



/**
  Start ABTastySdk with the given application identifier
 
  @param appId NSString Access key provided by AB Tasty
 
  @param pBlock The block to be invoked when sdk is ready
 */
- (void)startABTastySdk:(NSString*_Nonnull)appId WithCompletionBlock:(void (^_Nonnull)(void))pBlock;



/**
* This Start lets you to set common default tracking data to be shared between all your events.

 @param appId NSString Access key provided by AB Tasty

 @param pInfosTracking NSDictionary tracking information of common default data

 @warning See documentation "Common Tracking Information"
 
*/
- (void)startABTastySdk:(NSString*_Nonnull)appId WithTrackingInfos:(NSDictionary*_Nonnull)pInfosTracking;


/**
  This Start lets you to set common default tracking data to be shared between all your events.
 
  @param appId NSString Access key provided by AB Tasty
 
  @param pInfosTracking NSDictionary tracking information of common default data
 
  @param pBlock The block to be invoked when sdk is ready
 
  @warning See documentation "Common Tracking Information"
 
 */
- (void)startABTastySdk:(NSString*_Nonnull)appId WithTrackingInfos:(NSDictionary*_Nonnull)pInfosTracking WithCompletionBlock:(void (^_Nonnull)(void))pBlock;



/**
  Execute Block When the SDK is ready to use
 
  @param pBlock The block to be invoked when sdk is ready
 */
- (void)executeWhenSdkReady:(void (^_Nonnull)(void))pBlock;


///-----------------------------------------
/// @name Get Variations
///-----------------------------------------


/**
  Get Variation for campaign id
 
  @param pCampaignId NSString id for the campaign
 
  @return NSString corresponding to the selected variation, or nil
 
 */
- (nullable NSString*)getVariationForCampaignId:(NSString* _Nonnull )pCampaignId;




///-----------------------------------------
/// @name Activate Campaign
///-----------------------------------------


/**
  Activate campaign
 
  @param pCampaignId NSString  id for the campaign
 
  @param pVariationID NSString id for the variation
 
  @param pInterfaceName NSString interface name,the location where the campaign is activated
 
  @warning by default this campaign will be activated with common tracking information set when starting the sdk
 */
- (void)activateCampaign:(NSString*_Nonnull)pCampaignId forVariationId:(NSString*_Nonnull)pVariationID atInterfaceName:(NSString*_Nonnull)pInterfaceName;



/**
  Activate a campaign with another common tracking data
 
  @param pCampaignId NSString  id for the campaign
 
  @param pVariationID NSString id for the variation
 
  @param pTracking ABTracking Object that represents ommon tracking information
 */
- (void)activateCampaign:(NSString*_Nonnull)pCampaignId forVariationId:(NSString*_Nonnull)pVariationID withTracking:(ABTracking*_Nonnull)pTracking;


///-----------------------------------------
/// @name Tracking Data
///-----------------------------------------

/**
  Send Events
 
  @param pEventObject event object
 
  @warning this fucntion send all the events : ABTransaction, ABActionTracking, ABVisitEvent, ABCustomEvent
 
 */
- (void)sendTrackingEvent:(id<ABTrackingProtocol>_Nonnull)pEventObject;



///-----------------------------------------
/// @name Update tracking informations
///-----------------------------------------

/**
  Update Common tracking information shared by all events
 
  @param pInfoTracking NSDictionary
 
 */
- (void)updateConfigInfosTracking:(NSDictionary*_Nonnull)pInfoTracking;



@end


