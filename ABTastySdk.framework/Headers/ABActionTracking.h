//
//  ABActionTracking.h
//  ABTastySdk
//
//  Created by ABTasty on 31/01/2018.
//  Copyright © 2018 ABTasty. All rights reserved.
//

#import "ABTracking.h"
#import <UIKit/UIKit.h>




/**
 `ABActionTracking` class helps you create event object
 */

@interface ABActionTracking : ABTracking

 /**
 Label of the click tracking/touch as displayed in the reporting. required
 */
@property (nonatomic, strong) NSString * name;


/**
 Object that contains the position where the touch occurred in the mobile screen. required
 */
@property (nonatomic, assign) CGPoint position;


/**
 Value attached to the action tracking event. Only NSString or NSNumber are accpeted. optional
 */
@property (nonatomic, strong) id value;

@end
