# Welcome

Welcome to the AB Tasty server side **ABTastySdk** documentation!

The ABTastySDK project is an iOS framework that helps you create server side tests on your native iOS app.

You can use this framework to access AB Tasty endpoints, which can generate a unique visitor ID, allocate a visitor to a test, and push visit and conversion events in order to help you analyze the outcomes of your campaigns.

Should you have any question about this documentation

# App prerequisites

* Your app must be a native app written in Swift or Objective C.

* ABTastySdk supports at least ios 8.0+

# Getting started

ABtastySDK is available for distribution through Pods or manual installation.

### Cocoapods

> Make sure  `use_frameworks!`  is also included in the Podfile

```ruby
target 'Your App' do
	use_frameworks!
	
	   pod 'ABTastySdk'
  end
```

> Run pod install from your Xcode project's base directory

```shell
pod install

```
 
1. <a href='https://guides.cocoapods.org/'>Install CocoaPods.</a>

2. Open Terminal and navigate to the directory that contains your project then enter the following command: `pod init`

3. Open your Podfile and add the following line to the Podfile 

4. Run `pod install` from your Xcode project's base directory

5. Make sure you always open the Xcode workspace and not the project file when building your project

### Manual Installation

1. Download the frameWork  <a href='http://sdk.abtasty.com/ios/ABTastySdk.zip'>ABTastySdk.framework.</a>

2. In Finder, navigate to the ABTastySdk.framework file and move it under your "Embedded Binaries" section in Xcode

3. When moving it, a popup is displayed, in which you must check "Copy items if needed".

### Apple Store Submission

If your app contains a universal framework, the App store will reject your app because of an unwanted architecture.
You need to add a new build phase, then select `Run Script` to add a new build step after `Embed frameworks`.
In the `Shell` field, enter the following script:

```shell
FRAMEWORK="ABTastySdk"
FRAMEWORK_EXECUTABLE_PATH="${BUILT_PRODUCTS_DIR}/${FRAMEWORKS_FOLDER_PATH}/$FRAMEWORK.framework/$FRAMEWORK"
EXTRACTED_ARCHS=()
for ARCH in $ARCHS
do
lipo -extract "$ARCH" "$FRAMEWORK_EXECUTABLE_PATH" -o "$FRAMEWORK_EXECUTABLE_PATH-$ARCH"
EXTRACTED_ARCHS+=("$FRAMEWORK_EXECUTABLE_PATH-$ARCH")
done
lipo -o "$FRAMEWORK_EXECUTABLE_PATH-merged" -create "${EXTRACTED_ARCHS[@]}"
rm "${EXTRACTED_ARCHS[@]}"
rm "$FRAMEWORK_EXECUTABLE_PATH"
mv "$FRAMEWORK_EXECUTABLE_PATH-merged" "$FRAMEWORK_EXECUTABLE_PATH"
```

<aside class="notice">
<b>ABTastySdk FrameWork is universal and BitCode supported</b> <br>
The universal framework for iOS contains architectures for simulators and devices. You will then be able to run your application on all devices and all iOS simulators.
</aside>


## Emergency situations

If you are facing an emergency situation in your App, for example, you notice a crash and you suspect the ABTasty SDK, you can disable it via <a href='https://app2.abtasty.com/settings/framework'>settings</a> in the framework section of your account without republishing your App.

### How does it works

The status is set in the response header of each request sent from the SDK. Then, if you decide to deactivate it via your account settings, the SDK will automatically switch OFF at run time by the next request. Please note that it may take several minutes to apply while our servers process the new status.

<br>
<br>

The SDK sends requests :

 - At initialization time and only the first time in order to generate a unique visitor ID
 - At each initialization time in order to get allocations for campaigns
 - At run time when you send a ABTracker

So, the SDK will check at each initialization if its status has changed. When disabled, the initialization will stop and the SDK will be OFF until the next initialization.

Note that if the device is offline during the check, it will keep its previous status.


### Reactivation

As the communication is OFF when disabled, the reactivation will only be possible when you reactivate the option in your account settings and the SDK is initialized again. Please note that it may take several minutes to apply while our servers process the new status.

<aside class="notice">
This feature is available from the version 1.1.0 of the SDK
</aside>

<aside class="notice">
Please note that the change of status may take several minutes to apply on the app side
</aside>

# Starting campaigns

This section of the documentation aims at helping you to start a new campaign in your app with the ABTasty IOS SDK.

## SDK Initialization

### Initialize ABTastySDK

> import ABTastySdk frameWork and Add the following code into your AppDelegate.m file

```objective_c
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [[ABTasty sharedInstance] startABTastySdk:@"YOUR_ACCESS_KEY"];

    return YES;
}
```

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    ABTasty.sharedInstance().startSdk("YOUR_ACCESS_KEY")
    
	return true
}
```

> Initialize ABTastySDK with completion Block

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
    ABTasty.sharedInstance().startSdk("YOUR_ACCESS_KEY") {

        print("SDK is Ready")
    }
    return true
}
```

```objective_c
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[ABTasty sharedInstance] startABTastySdk:@"YOUR_ACCESS_KEY" WithCompletionBlock:^{
        
        NSLog(@"SDK is Ready");
	}];

	return YES;
}
```



To run experiments with ABTasty you'll need to initialize ABTasty with the AB_TASTY_KEY parameter. ABTasty uses a sharedInstance that can activate experiments and track events.

To do so, just call the `startABTastySdk` function in the ABTasty class located in the framework ABTastySdk.framework. For each new user, during the very first initialization, the SDK requests a new visitor ID to our servers and saves it in `UserDefaults`, and for each initialization the SDK will allocate variations for all camapaigns.

`(void)startABTastySdk:(NSString*)appId`

Parameter | Type | Description
--------- | ------- | -----------
appId | NSString | Access key provided by AB Tasty


As the SDK is asynchronous and runs in parallel, this method `startABTastySdk:WithCompletionBlock:` lets you set a block which will be executed when the SDK is ready. 
This delay depends on the network, once it is ready, the ABTest is ready. Note that this block does not run on the main thread 

`- (void)startABTastySdk:(NSString*)appId WithCompletionBlock:(void (^)(void))pBlock`

Parameter | Type | Description
--------- | ------- | -----------
appId | NSString | Access key provided by AB Tasty
pBlock | block | The block to be invoked



<aside class="notice">
Replace <code>AB_TASTY_KEY</code> with the key provided by AB Tasty.
</aside>
<br><br>

### Initialize with Tracking data

> Initialize with Tracking data

```objective_c
 [[ABTasty sharedInstance] startABTastySdk:@"YOUR_ACCESS_KEY" WithTrackingInfos:
    @"Longitude":@2.3333,
    @"Latitude": @48.48,
    @"ipAddress":@"xxx.xxx.xxx.xxx"
    }];
```

```swift
     ABTasty.sharedInstance().startSdk("YOUR_ACCESS_KEY", withTrackingInfos:
         ["Longitude":2.3333, "Latitude":48.48,,"ipAddress":"xxx.xxx.xxx.xxx"])
```
`- (void)startABTastySdk:(NSString*)appId WithTrackingInfos:(NSDictionary*)pInfosTracking`

This method lets you to set common default tracking data to be shared between all your events.

Parameter | Type | Description
--------- | ------- | -----------
appId | NSString | Access key provided by AB Tasty
pInfosTracking | NSDictionary | tracking information of common default data

###Common Tracking Information

Parameter | Type | Description
--------- | ------- | -----------
Longitude | Double     | Longitude
Latitude  | Double | Latitude
ipAddress | NSDictionary | ip adress

> You can update all parameters or one of them

```objective_c

// Update all common tracking data
[[ABTasty sharedInstance] updateConfigInfosTracking:@{@"Longitude":@2.352222,
 @"Latitude": @48.856614,
@"ipAddress":@"xxx.xxx.xxx.xxx"}];
	
// Update one of the common tracking data - ipAdress
[[ABTasty sharedInstance] updateConfigInfosTracking:@{@"ipAddress":@"xxx.xxx.xxx.xxx"}];

```
```swift

// Update all common tracking information data
ABTasty.sharedInstance().startSdk("AIzaSyBOtlLZ1TPTmS1HJYvL1xVuiB5GndXD4P8", withTrackingInfos:
["Longitude":2.33, "Latitude":48.48,"ipAddress":"xxx.xxx.xxx.xxx"])
		
// Update one of the common tracking information data - ipAddress
ABTasty.sharedInstance().updateConfigInfosTracking(["ipAddress":"xxx.xxx.xxx.xxx"])

```

You can update this tracking information by calling : `-(void)updateConfigInfosTracking:(NSDictionary*)pInfoTracking`

<br>
<aside class="notice">
By default the SDK sends all the events with <b>common tracking information</b> set when starting the SDK.
</aside>

<br>
<aside class="notice">
If you want to send an event with other common information use the <b>initABTracking</b> function to create the event. The SDK will ignore the common tracking information used by default.
</aside>

`- (instancetype)initABTracking:(ABDeviceType)pDeviceType longtitude:(double)pLong latitude :(double)pLat ipAddress :(NSString*)pAdressIp interfaceName:(NSString*)pInterfaceName`


<aside class="success">
Congratulations !!
<br><br>
You should now be able to use the ABTastySdk :)
</aside>


## Getting variations

> Get Variation for Campaign

```objective_c
    NSString *variationId = [[ABTasty sharedInstance] getVariationForCampaignId:@"xxxxx"];
    
    if([variationId isEqualToString:@"aaaaa"]){

		// Manage your Variation aaaaa
 
    }else if ([variationId isEqualToString:@"bbbbb"]){ 

		// Manage your Variation bbbbb

    }else{ 

		// The default behavior
    }
```

```swift
        
    let variationId = ABTasty.sharedInstance().getVariationForCampaignId("xxxxx")
        
        if variationId == "aaaaa" {

			// Manage your Variation aaaaa
                
        }else if variationId == "bbbbb" {

			// Manage your Variation bbbbb

        }else{

			// The default behavior
		}
```

When the SDK is ready to use, all campaigns will have an allocated variation that determines which of your campaign's variations they are going to see

To get this information use the `getVariationForCampaignId` function in the ABTasty class located in the framework ABTastySdk.framework.

This function return an NSString corresponding to the selected variation, or `nil` if :

- The campaign doesn't exist,
- The campaign is not running 
- The SDK is not ready to use

You should activate the campaign when you apply some changes for the variation, so that the user will have the same behavior

`- (NSString*)getVariationForCampaignId:(NSString*)pCampaignId`

Parameter | Type | Description
--------- | ------- | -----------
pCampaignId | NSString | campaign id 

<aside class="notice">
Replace xxxxxx with your campaign ID and, aaaaaa and bbbbbb with the different variation IDs of your campaign.
</aside>

### Block Operation When SDK is Ready

> Execute Block when SDK is ready

```objective_c

    [ABTasty.sharedInstance executeWhenSdkReady:^{
        
        NSString * variationId = [[ABTasty sharedInstance] getVariationForCampaignId:@"xxxxx"];
        
        if ([variationId isEqualToString:@""]) {
            
            // Manage variation aaaaa
            
        }else if ([variationId isEqualToString:@""]){
            
            // Manage variation bbbbb
            
        }else{
            
            // The default behavior
        }
    }];

```

```swift

    ABTasty.sharedInstance().execute {
            
        let variation = ABTasty.sharedInstance().getVariationForCampaignId("xxxxx")
            
        if variation == "aaaaa" {
                
            // Manage your variation aaaaa
                
                
        }else if variation == "bbbbb" {
                
            // Manage your variation bbbbb

        }else{
                
            // The default behavior
        }
    }

```

This function allows users to invoke a block when the SDK is ready after campaigns have been fetched from the server side. Note that this block is not running on the main thread

`- (void)executeWhenSdkReady:(void (^)(void))pBlock`

Parameter | Type | Description
--------- | ------- | -----------
pBlock    | block | The block to be invoked

## Activate Campaign


> Activate campaign

```objective_c
[[ABTasty sharedInstance] activateCampaign:@"campaignId" forVariationId:varId atInterfaceName:@"ABHomeViewController"];
```

```swift
ABTasty.sharedInstance().activateCampaign("campaignId", forVariationId: "varId", atInterfaceName:"ABHomeViewController")
```
> Activate campaign with ABTracking Object

```objective_c
// Tracking Object
ABTracking * tracking = [[ABTracking alloc] initABTracking:ABMobile longtitude:2.33 latitude:48.48 ipAddress:@"xxx.xxx.xxx.xxx" interfaceName:@"ABHomeViewController"];
		
// Activate campaign
[[ABTasty sharedInstance] activateCampaign:@"campaignId" forVariationId:@"varId" withTracking:tracking];

```

```swift
// Tracking Object
let trackig = ABTracking(abTracking:ABDeviceType.mobile, longtitude: 2.33, latitude: 48.33, ipAdress: "xxx.xxx.xxx.xxx", interfaceName: "ABHomeViewController")

// Activate Campaign "campaignId"
ABTasty.sharedInstance().activateCampaign("campaignId", forVariationId: "variationId", with:trackig)
```

`-(void)activateCampaign:(NSString*)pCampaignId forVariationId:(NSString*)pVariationID atInterfaceName:(NSString *)pInterfaceName`

Parameter | Type | Description
--------- | ------- | -----------
pCampaignId | NSString | campaign id 
pVariationID | NSString | variation id
pInterfaceName | NSString | interface name, the location where the campaign is activated 


Once a variation has been allocated to a campaign you need to activate it. This way it tells AB Tasty that the current user saw this specific variation of the campaign so that the user
always sees the same variation.

The allocation can be changed if you call `allocateVariation` several times until you use `activateCampaign` to activate the campaign and freeze the variation.

This **activate** function uses common tracking information if it is already set before starting the SDK

If you want to activate a campaign with other common tracking data, use this function :

`- (void)activateCampaign:(NSString*)pCampaignId forVariationId:(NSString*)pVariationID withTracking:(ABTracking*)pTracking`

Parameter | Type | Description
--------- | ------- | -----------
pCampaignId | NSString | campaign id 
pVariationID | NSString | variation id
pTracking | ABTracking | Object that represents common tracking information 


# Sending events

The library also provides some functions to send events to ABTasty, for example to send goals configured for a campaign.

To do so, use the `sendTrackingEvent` method and specify the expected tracker as a parameter.

All existing trackers are listed below.

 
## ABTracking

ABTracking is the generic tracker that contains common data and from which all others trackers inherit from

Parameter | Type | Description
--------- | ------- | -----------
device_type | ABDeviceType | Device used by the visitor to view the test. The devices currently supported  are: `"MOBILE"`, `"TABLET"`. **required**
interfaceName | String | interface or page from where the event has been created **required**
longitude | double | longitude of the device for geolocation  **optional**
latitude  | double | latitude  of the device for geolocation  **optional**
ipAddress | String | current ip of the device **optional**

<aside class="notice">
Common data can be automatically set from the ABTracking passed in the `startABTastySdk` when starting the SDK, or from another ABTracking instance.
</aside>

<br>

## Action Tracking

> Send Action Tracking Event

```objective_c
ABActionTracking * action = [[ABActionTracking alloc] initABTracking:ABMobile longtitude:2.33 latitude:48.48 ipAdress:@"912.168.1.1" interfaceName:@"ABHomeViewController"];
action.name  = @"MyGoal";
action.position = CGPointMake(100, 200);

action.value = @"value";	
// Send Action Tracking	
[[ABTasty sharedInstance] sendTrackingEvent:action];
```

```swift
let action :ABActionTracking = ABActionTracking(abTracking:ABDeviceType.mobile, longtitude: 2.33, latitude: 48.48, ipAdress: "192.168.1.1", interfaceName: "ABHomeViewController")		
action.name = "MyGoal"
action.value = "value"
action.position = CGPoint(x: 100, y: 200)
// Send Action tracking
ABTasty.sharedInstance().sendTrackingEvent(action)
```

**ABActionTracking**, This tracker sends a touch event

Parameter | Type | Description
--------- | ------- | -----------
name | String | Label of the click tracking as displayed in the reporting. **required**
value  | id | value attached to the action tracking event. Only NSString or NSNumber are accpeted. **optional**
position | CGPoint | Struct that contains the position x/y where the touch occurred in the mobile screen. **required**

<br>

## Custom Tracking

```objective_c
ABCustomEvent * customEvent =  [[ABCustomEvent alloc] initABTracking:ABMobile longtitude:2.33 latitude:48.48 ipAdress:@"xxx.xxx.xxx.xxx" interfaceName:@"ABHomeViewController"];

[customEvent setValue:@"value"];

[customEvent setGoal_name:@"GoalName"];

[[ABTasty sharedInstance] sendTrackingEvent:customEvent];
```

```swift
let custom:ABCustomEvent = ABCustomEvent(abTracking: ABDeviceType.mobile, longtitude: 2.33, latitude: 48.48, ipAdress: "xxx.xxx.xxx.xxx", interfaceName: "ABHomeViewController")

custom.goal_name = "goal"

custom.value = "value"

ABTasty.sharedInstance().sendTrackingEvent(custom)
```

**ABCustomEvent** This tracker sends a custom event and lets you track events that are specific to your business.

Parameter | Type | Description
--------- | ------- | -----------
goal_name | String | Label of the custom event tracked as displayed in the reporting . **required**
value | String | String value attached to the action tracking event. **optional**

<br>

## Transaction tracking

```objective_c

ABTransaction * transaction = [[ABTransaction alloc] initABTracking:ABMobile longtitude:2.33 latitude:48.48 ipAdress:@"xxx.xxx.xxx.xxx" interfaceName:@"ABHomeViewController"];

transaction.name = @"transaction";
transaction.transaction_id = @"revenue";
transaction.revenue = 1000;
transaction.shipping = 12;
transaction.tax_total = 0.23;
transaction.discount = 0.5;
transaction.payment_method = @"PayPal";
transaction.currencyName = @"currency_name";
transaction.currencyRate = 0.222;
transaction.items_count = 12;
    
// Set micro transaction
ABMicroTransaction * minTransaction = [[ABMicroTransaction alloc] init];
minTransaction.name = @"name";
minTransaction.price = 12;
minTransaction.sku = @"sku";
minTransaction.category = @"cat";
minTransaction.quantity = 2;
minTransaction.tax_amount = 1111;
transaction.microTransaction = [NSMutableArray arrayWithObject:minTransaction];
```

```swift
let transaction:ABTransaction = ABTransaction(abTracking: ABDeviceType.mobile, longtitude: 2.33, latitude: 48.33, ipAdress:"xxx.xxx.xxx.xxx", interfaceName: "ABHomeViewController")
		
transaction.name = "transaction"
transaction.transaction_id = "revenue"
transaction.revenue = 1000
transaction.shipping = 12
transaction.tax_total = 0.23
transaction.discount = 0.5
transaction.payment_method = "PayPal"
transaction.currencyName = "currency_name"
transaction.currencyRate = 0.222
transaction.items_count = 12

// Set micro transaction
let miniTransaction:ABMicroTransaction = ABMicroTransaction()
miniTransaction.name = "name"
miniTransaction.price = 12
miniTransaction.sku = "sku"
miniTransaction.category = "cat"
miniTransaction.quantity = 2
miniTransaction.tax_amount = 1111
transaction.microTransaction = [miniTransaction];
ABTasty.sharedInstance().sendTrackingEvent(transaction)
```

The **ABTransaction** lets you track any purchase on your mobile app.

Parameter | Type | Description
--------- | ----------- | -----------
name | string | Label of the Transaction as displayed in the reporting.**required**
transaction_id | string |Unique identifier of the transaction.**required**
revenue | double |Total amount of money spent by the visitor expressed in cents. **required**
shipping | double |Shipping cost - expressed in cents.**required**
tax_total | double |Total tax expressed in cents. **optional**
discount | double |Discount in cents, if applicable. **optional**
payment_method | string | Means of payment. **optional**
items_count | double | Total number of products purchased on the mobile app. **optional**
currencyName | string | Name of the money used for the payment. **required**
currencyRate | float | Change rate between USD in cents (used as a reference) and the money used for the payment. **required**
microTransaction|ABMicroTransaction | Products purchased on the web site/mobile app. **optional**

**ABMicroTransaction**

Parameter | Type | Description
--------- | ----------- | -----------
name | string | Label of the product.  **required**
price | int | Price of the item expressed in cents.**required**
sku | string | Identifier of the item. **optional**
category | string | Category of product the item belongs to.
quantity | int | Number of identical products purchased at the same time on the mobile app. **optional**
tax_amount | int | Tax amount applied to the item. **optional**
tax_rate | float | Percentage of tax applied to the item. **optional**

<br>

## Visit tracking

```objective_c
ABVisitEvent * visitEvent = [[ABVisitEvent alloc] initABTracking:ABMobile longtitude:2.33 latitude:48.33 ipAdress:@"xxx.xxx.xxx.xxx" interfaceName:@"ABHomeViewController"];

[[ABTasty sharedInstance] sendTrackingEvent:visitEvent];

```
```swift
let visitEvent:ABVisitEvent = ABVisitEvent(abTracking: ABDeviceType.mobile, longtitude: 2.33, latitude: 48.33, ipAdress:"xxx.xxx.xxx.xxx", interfaceName: "ABHomeViewController")
     
 ABTasty.sharedInstance().sendTrackingEvent(visitEvent)
```

The **ABVisitEvent** lets you track all visits that occurr on a mobile app.

No parameters are needed for this event as the SDK uses the `COMMON TRACKING INFORMATIONS` set previously or when starting the SDK.
<br>


# Reference

  <a href='http://sdk.abtasty.com/ios/ref/index.html'>Reference</a>


# Releases

### Version 1.1.0 (current stable version)

<ul>
<li>Emergency Feature
<li>Replace position_X, position_Y in action tracking Object by CGPoint
<li>Bug fixes
</ul>
